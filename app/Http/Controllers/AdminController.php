<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function index()
    {
        $title = 'Dashboard';
        $name = DB::table('users')->select('name')->get();

        return view('admin.index', compact('title', 'name'));
    }
}
