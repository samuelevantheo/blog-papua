<?php

namespace App\Http\Controllers;

use App\Models\ModelBlog;
use App\Models\ModelKota;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Blog';
        $data = ModelBlog::allData();

        return view('blog.index', compact('data', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Tambah data Blog';
        $data = ModelKota::all();

        return view('blog.create', compact('data', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'uploadgambar' => 'required',
            'masterkota' => 'required',
            'deskripsi' => ''
        ]);
        $document = $request->file('uploadgambar');
        $document_name = $document->getClientOriginalName();
        $doc_path = $document->store('public');

        $store = new ModelBlog();
        $store->nama_file = $document_name;
        $store->path_file = $doc_path;
        $store->deskripsi = $request->deskripsi;
        $store->reference = $request->masterkota;
        $store->save();

        return redirect()->route('blog.index')->with('success', 'Blog created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $blog
     * @return \Illuminate\Http\Response
     */
    public function show($blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($blog)
    {
        $title = 'Edit data Blog';
        $data = ModelKota::all();
        $now = ModelBlog::findOrFail($blog);
        $idkota = $now->reference;
        $namakota = DB::table('master_kota')->select('nama_kota')->where('id_master_kota', $idkota)->value('nama_kota');

        return view('blog.edit', compact('data', 'title', 'now', 'namakota', 'blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $blog
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $blog)
    {

        $request->validate([
            'uploadgambar' => 'required',
            'masterkota' => 'required',
            'deskripsi' => ''
        ]);
        $document = $request->file('uploadgambar');
        $document_name = $document->getClientOriginalName();
        $doc_path = $document->store('public');

        $update = ModelBlog::where('id_blog', $blog)->firstOrFail();
        $update->nama_file = $document_name;
        $update->path_file = $doc_path;
        $update->deskripsi = $request->deskripsi;
        $update->reference = $request->masterkota;
        $update->save();

        return redirect()->route('blog.index')->with('success', 'Blog updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($blog)
    {
        $data = ModelBlog::findOrFail($blog);

        $data->delete();

        return redirect()->route('blog.index')->with('success', 'Blog deleted successfully');
    }
}
