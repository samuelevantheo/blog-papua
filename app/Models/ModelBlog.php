<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ModelKota;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

class ModelBlog extends Model
{
    use HasFactory;
    protected $table = 'blog';
    protected $guarded = [];
    protected $primaryKey = 'id_blog';
    public $timestamps = true;

    public static function allData()
    {
        $data = DB::table('blog')->select('blog.*', 'master_kota.nama_kota')->leftJoin('master_kota', 'master_kota.id_master_kota', '=', 'blog.reference')->get();
        return $data;
    }
}
