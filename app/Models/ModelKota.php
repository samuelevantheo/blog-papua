<?php

namespace App\Models;

use App\Http\Controllers\BlogController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelKota extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'master_kota';
    protected $primaryKey = 'id_master_kota';
    public $timestamp = true;


    public function namakota()
    {
        return $this->hasMany(BlogController::class);
    }
}
