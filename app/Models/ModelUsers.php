<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ModelUsers extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamp = true;

    public static function allData()
    {
        $data = DB::table('users')
            ->where('role', 'user')
            ->get();
        return $data;
    }
}
