@extends('layouts.index')

@push('styles')

@endpush


@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
                <!--begin::User-->
                <div class="d-flex flex-column">
                    <!--begin::Name-->
                    <div class="d-flex align-items-center mb-2">
                        <h3>Data User</h3>
                    </div>
                    <!--end::Name-->
                </div>
                <!--end::User-->
                <!--begin::Actions-->
                <div class="d-flex my-4">
                    <a href="{{ route('usermg.create') }}" class="btn btn-sm btn-primary me-3"><i class="fas fa-add"></i> Tambah Data</a>
                </div>
                <!--end::Actions-->
            </div>
        </div>
        <div class="row">
            <div class="container">
                @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Registered</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->name }}</td> <!-- Updated line -->
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>
                                    <form action="{{ route('usermg.destroy', $user->id) }}" method="POST" class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn" onclick="return confirm('Apakah Anda yakin ingin menghapus blog ini')"><i class="fas fa-trash text-danger"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection