@extends('layouts.index')

@push('styles')

@endpush


@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="container">
                <form action="{{ route('usermg.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <br>
                    <div class="mb-3">
                        <label for="input_email" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="name" name="name" required>
                    </div>
                    <div class=" mb-3">
                        <label for="input_email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" required>
                    </div>
                    <div class="mb-3">
                        <label for="input_password" class="form-label">Password</label>
                        <input type="password" class="form-control" id="password" name="password" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection