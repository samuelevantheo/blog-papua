@extends('layouts.index')

@push('styles')

@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="container">
                <form action="{{ route('blog.update', $blog) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label for="uploadgambar" class="form-label">Upload gambar kota</label><span class="text-danger">*</span>
                        <input class="form-control" type="file" id="uploadgambar" name="uploadgambar" required>
                    </div>
                    <br>
                    <div class="mb-3">
                        <label for="masterkota">Pilih kota</label><span class="text-danger">*</span>
                        <select class="form-select" id="masterkota" name="masterkota" aria-label="Default select example" required>
                            <option disabled selected>{{ $namakota }}</option>
                            @foreach($data as $item)
                            <option value="{{ $item->id_master_kota }}">{{ $item->nama_kota }}</option>
                            @endforeach
                        </select>
                    </div>
                    <br>
                    <div class="mb-3">
                        <div class="form-floating">
                            <input class="form-control" type="text" placeholder="edit" value="{{ $now->deskripsi }}" id="deskripsi" name="deskripsi" aria-label="default input example">
                            <label for="deskripsi">Deskripsi</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection