/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 100424 (10.4.24-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : blog_papua

 Target Server Type    : MySQL
 Target Server Version : 100424 (10.4.24-MariaDB)
 File Encoding         : 65001

 Date: 14/06/2023 22:13:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for blog
-- ----------------------------
DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog`  (
  `id_blog` int NOT NULL AUTO_INCREMENT,
  `nama_file` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `path_file` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deskripsi` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `reference` int NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_blog`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- ----------------------------
-- Records of blog
-- ----------------------------
BEGIN;
INSERT INTO `blog` (`id_blog`, `nama_file`, `path_file`, `deskripsi`, `reference`, `created_at`, `updated_at`) VALUES (1, 'City_of_Manokwari.jpg', 'public/Vov6BGk0PjU0ZTJseri7KHcMtXAYOWIbQ8ObHbDX.jpg', 'Kota manokwari', 21, '2023-06-14 13:07:44', '2023-06-14 13:07:44'), (2, 'wisata-jayapura-5-Harian-Papua.png', 'public/bZJVzRrxAxhPCE4JBrUKPOmcRNvdg7kLyU3fjOIa.png', 'Pemandangan', 1, '2023-06-14 13:12:01', '2023-06-14 13:12:01'), (3, 'Danau-Uter-Maybrat-Papua-Barat-@2819purerelax.jpg', 'public/7n12ajDuZpapxbi2vhArjFY3KvJhwUyCNJypBs91.jpg', 'Danau Uter', 8, '2023-06-14 13:12:26', '2023-06-14 13:12:26');
COMMIT;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for master_kota
-- ----------------------------
DROP TABLE IF EXISTS `master_kota`;
CREATE TABLE `master_kota`  (
  `id_master_kota` int NOT NULL AUTO_INCREMENT,
  `nama_kota` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `deskripsi` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_master_kota`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- ----------------------------
-- Records of master_kota
-- ----------------------------
BEGIN;
INSERT INTO `master_kota` (`id_master_kota`, `nama_kota`, `deskripsi`, `created_at`, `updated_at`) VALUES (1, 'Jayapura', NULL, '2023-06-14 10:43:12', NULL), (2, 'Serui Kota', NULL, '2023-06-14 10:43:12', NULL), (3, 'Distrik Wamena', NULL, '2023-06-14 10:43:12', NULL), (4, 'Dekai', NULL, '2023-06-14 10:43:12', NULL), (5, 'Yetty', NULL, '2023-06-14 10:43:12', NULL), (6, 'Sarmi', NULL, '2023-06-14 10:43:12', NULL), (7, 'Mimika', NULL, '2023-06-14 10:43:12', NULL), (8, 'Maybrat', NULL, '2023-06-14 10:43:12', NULL), (9, 'Sorong', NULL, '2023-06-14 10:43:12', NULL), (10, 'Agats', NULL, '2023-06-14 10:43:12', NULL), (11, 'Tanahmerah', NULL, '2023-06-14 10:43:12', NULL), (12, 'Nabire', NULL, '2023-06-14 10:43:12', NULL), (13, 'Manokwari Selatan', NULL, '2023-06-14 10:43:12', NULL), (14, 'Enarotali', NULL, '2023-06-14 10:43:12', NULL), (15, 'Pegunungan Arfak', NULL, '2023-06-14 10:43:12', NULL), (16, 'Merauke', NULL, '2023-06-14 10:43:12', NULL), (17, 'Oksibil', NULL, '2023-06-14 10:43:12', NULL), (18, 'Bupul', NULL, '2023-06-14 10:43:12', NULL), (19, 'Getentiri', NULL, '2023-06-14 10:43:12', NULL), (20, 'Mindiptana', NULL, '2023-06-14 10:43:12', NULL), (21, 'Manokwari', NULL, '2023-06-14 10:43:12', NULL), (22, 'Moanemani/Ikebu', NULL, '2023-06-14 10:43:12', NULL), (23, 'Kepi', NULL, '2023-06-14 10:43:12', NULL), (24, 'Safan, Asmat', NULL, '2023-06-14 10:43:12', NULL), (25, 'Teminabuan', NULL, '2023-06-14 10:43:12', NULL), (26, 'Mpur, Tambrauw', NULL, '2023-06-14 10:43:12', NULL), (27, 'Distrik Kwoor', NULL, '2023-06-14 10:43:12', NULL), (28, 'Distrik Inanwatan', NULL, '2023-06-14 10:43:12', NULL), (29, 'Distrik Suasapor', NULL, '2023-06-14 10:43:12', NULL), (30, 'Distrik Fakfak', NULL, '2023-06-14 10:43:12', NULL), (31, 'Distrik Ransiki', NULL, '2023-06-14 10:43:12', NULL), (32, 'Distrik Kota Waisai', NULL, '2023-06-14 10:43:12', NULL);
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (1, '2014_10_12_000000_create_users_table', 1), (2, '2014_10_12_100000_create_password_resets_table', 1), (3, '2019_08_19_000000_create_failed_jobs_table', 1), (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
COMMIT;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token` ASC) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type` ASC, `tokenable_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'user',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role`) VALUES (1, 'Admin', 'admin@admin.com', NULL, '$2y$10$XfTIBC2JXsbniQWnb1ncYuG9rwQaujli4KEvNWzSxnHShLP1GkRZy', NULL, '2023-06-14 03:58:35', '2023-06-14 03:58:35', 'admin'), (5, 'Shanks', 'shanks@gmail.com', NULL, '$2y$10$dvhTQgUGOkON9AzmwS9.CulWIrcBakt3Jqzy58Szfkz0TXisCXZY6', NULL, '2023-06-14 13:11:25', '2023-06-14 13:11:25', 'user');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
